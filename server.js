import { app } from "./src/app.js";
import { configuration } from "./src/config/configuration.js"

app.listen(configuration.server.port, (err)=>{
    if(err)
        throw err
    console.log('Server has been running on port ', configuration.server.port);
})
