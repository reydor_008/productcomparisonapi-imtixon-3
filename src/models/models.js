import db from "../config/knexConnect.js";
import { v4 as uuidv4 } from 'uuid';
import knex from "knex";

export default class PgModel {
    constructor() {
        this.#createUsersTable();
        this.#createReviewsTable(); // Corrected typo in function name
        this.#createProductsTable(); // Corrected typo in function name
        this.#createTableOtp()
    }

    async #createUsersTable() {
        const exists = await db.schema.hasTable('users');
        if (!exists) {
            await db.schema.createTable('users', (table) => {
                table.uuid('id').primary().defaultTo(uuidv4()); // Use database function for UUID
                table.string('first_name').notNullable();
                table.string('last_name').notNullable();
                table.string('email', 255).notNullable().unique();
                table.string('username', 255).notNullable().unique();
                table.string('password', 255).notNullable();
                table.enum('role', ['user', 'admin', 'moderator']).defaultTo('user');
                table.enum('status', ['active', 'inactive']).defaultTo('inactive');
                table.timestamp('created_at', { precision: 6 }).defaultTo(db.fn.now(6));
                table.string('updated_at', 255).defaultTo('null');
                table.string('refresh_token', 255).defaultTo('null')
                // Use timestamp for consistency
            });
            console.log('User table created');
        } else {
            console.log('User table already exists');
        }
    }

    async #createProductsTable() {
        const exists = await db.schema.hasTable('products');
        if (!exists) {
            await db.schema.createTable('products', (table) => {
                table.uuid('id').primary().defaultTo(uuidv4());
                table.string('name', 255).notNullable().unique();
                table.text('description').notNullable();
                table.string('category').notNullable();
                table.decimal('price', 14, 2).notNullable(); // Use decimal for monetary values
                table.timestamp('created_at', { precision: 6 }).defaultTo(db.fn.now(6));
                table.string('updated_at').defaultTo('null')

            });
            console.log('Product table created');
        } else {
            console.log('Product table already exists');
        }
    }

    async #createReviewsTable() { // Corrected function name
        const exists = await db.schema.hasTable('reviews'); // Changed to 'reviews'
        if (!exists) {
            await db.schema.createTable('reviews', (table) => {
                table.uuid('id').primary().defaultTo(uuidv4());
                table.uuid('product_id').notNullable()
                    .references('id').inTable('products')
                    .onDelete('CASCADE'); 
                table.uuid('user_id').notNullable()
                    .references('id').inTable('users')
                    .onDelete('CASCADE'); 
                table.integer('rating').notNullable().checkBetween([1, 5]); // Use check constraint for rating
                table.text('context').notNullable(); // Fixed typo from 'contex' to 'context'
                table.enum('status', ['approved', 'pending', 'rejected']).defaultTo('approved');
                table.timestamp('created_at', { precision: 6 }).defaultTo(db.fn.now(6));
                table.string('updated_at').defaultTo('null')
            });
            console.log('Review table created'); // Changed log message
        } else {
            console.log('Review table already exists'); // Changed log message
        }
    }
    async #createTableOtp(){
        const exists = await db.schema.hasTable('otp'); // Changed to 'reviews'
        if(!exists){
            await db.schema.createTable('otp', function(table) {
                table.increments('id').primary();
                table.string('user_id', 50).notNullable();
                table.string('code', 50).notNullable();
                table.timestamp('sended_time', { useTz: true, precision: 6 }).defaultTo(db.fn.now(6));
            });
            console.log('otp table created'); // Changed log message

        } else {
            console.log('otps table already exists'); // Changed log message
        }
    }
}
