class ApiError extends Error {
    constructor(status, message) {
        super(message);
        this.status = status;
        this.message = message;
    }

    static badRequest(res, errorMessage, friendlyMsg = "Bad Request") {
        return res.status(400).json({
            message: errorMessage,
            friendlyMsg: friendlyMsg
        });
    }

    static unauthorized(res, errorMessage, friendlyMsg = "Unauthorized") {
        return res.status(401).json({
            message: errorMessage,
            friendlyMsg: friendlyMsg
        });
    }

    static forbidden(res, errorMessage, friendlyMsg = "Forbidden") {
        return res.status(403).json({
            message: errorMessage,
            friendlyMsg: friendlyMsg
        });
    }

    static notFound(res, errorMessage, friendlyMsg = "Not Found") {
        return res.status(404).json({
            message: errorMessage,
            friendlyMsg: friendlyMsg
        });
    }

    static internal(res, errorMessage, friendlyMsg = "Internal Server Error") {
        return res.status(500).json({
            message: errorMessage,
            friendlyMsg: friendlyMsg
        });
    }

    static token_expired(res, errorMessage, friendlyMsg = "Your acces token expired"){
        return res.status(402).json({
            message : errorMessage,
            friendlyMsg : friendlyMsg
        })
    }
}

export default ApiError;
