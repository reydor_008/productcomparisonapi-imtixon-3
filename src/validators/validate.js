import Joi from 'joi';


export const userValidate = async(data) =>{
    const userSchema = Joi.object({
        first_name : Joi.string().required(),
        last_name : Joi.string().required(),
        email: Joi.string().email().required(),
        username: Joi.string().alphanum().min(3).max(30).required(),
        password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).required(),
        confirmPassword : Joi.ref('password'),
        role: Joi.string().valid('user', 'admin', 'moderator').default('user'),
    });
    return userSchema.validate(data)
} 


export const productValidate = async(data) =>{
    const productSchema = Joi.object({
        name: Joi.string().required(),
        description: Joi.string().required(),
        category: Joi.string().required(),
        price: Joi.number().min(0).required(),
    });
    return productSchema.validate(data)
}

export const reviewValidate = async(data) =>{
    const reviewSchema = Joi.object({
        product_id: Joi.string().guid({ version: 'uuidv4' }).required(),
        // user_id: Joi.string().guid({ version: 'uuidv4' }).required(),
        rating: Joi.number().integer().min(1).max(5).required(),
        context: Joi.string().required(),
        // status: Joi.string().valid('approved', 'pending', 'rejected').default('approved'),
    });
    return reviewSchema.validate(data)
}
