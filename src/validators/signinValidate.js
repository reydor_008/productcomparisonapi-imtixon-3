import Joi from "joi";

export const signInValidate = (data) => {
    const schema = Joi.object({
        password: Joi.string().pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')).required(),
        email: Joi.string().email().required(),  
    })

    return schema.validate(data)
}