import db from '../config/knexConnect.js';
import { v4 as uuidv4 } from 'uuid';

export default class ReviewService {
    static async createReview(reviewData) {
        const id = uuidv4();
        const created_at = new Date().toISOString();
        reviewData.id = id
        reviewData.created_at = created_at
        console.log(reviewData)
        try {
            await db('reviews').insert(
                reviewData
            );
            return reviewData;
        } catch (error) {
            throw new Error(`Failed to create review: ${error.message}`);
        }
    }

    static async getReviewById(id) {
        try {
            const review = await db('reviews').where({ id }).first();
            return review;
        } catch (error) {
            throw new Error(`Failed to fetch review: ${error.message}`);
        }
    }
    static async getReview() {
        try {
            const review = await db.select('*').from('reviews');
            return review;
        } catch (error) {
            throw new Error(`Failed to fetch review: ${error.message}`);
        }
    }

    static async updateReview(id, updates) {
        const { rating, context, status } = updates;
        const updated_at = new Date().toISOString();
        try {
            await db('reviews')
                .where({ id })
                .update({
                    rating,
                    context,
                    status,
                    updated_at
                });
            return await this.getReviewById(id); // Return updated review
        } catch (error) {
            throw new Error(`Failed to update review: ${error.message}`);
        }
    }

    static async deleteReview(id) {
        try {
            const review = await this.getReviewById(id);
            if (!review) {
                return false;
            }
            await db('reviews').where({'id' : id }).del();
            return true;
        } catch (error) {
            throw new Error(`Failed to delete review: ${error.message}`);
        }
    }
}
