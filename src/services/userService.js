import db from '../config/knexConnect.js';
import { v4 as uuidv4 } from 'uuid';

export default class UserService {
    static async createUser(userData) {
        const id = uuidv4();
        const created_at = new Date().toISOString();
        userData.id = id
        userData.created_at = created_at
        console.log(userData)

        try {
            await db('users').insert(userData);
            return  userData ;
        } catch (error) {
            throw new Error(`Failed to create user: ${error.message}`);
        }
    }

    static async getUserById(id) {
        try {
            const user = await db('users').where({ id }).first();
            return user;
        } catch (error) {
            throw new Error(`Failed to fetch user: ${error.message}`);
        }
    }
    
    static async getAllUser(){
        let users = await db.select('*').from('users')
        return users
    }
    
    static async activateUser(userId) {
        try {
            await db('users')
                .where({ id: userId })
                .update({ status : 'active' });

            // Optionally, you can delete the OTP record after verification
            await db('otp')
                .where({ user_id: userId })
                .del();

        } catch (error) {
            throw new Error(`Failed to activate user: ${error.message}`);
        }
    }

    static async getUserByEmailAndUsername(email, username) {
        try {
            const user = await db('users').where({'email' : email}).orWhere({'username':username}).first();
            return user || null; // Return null if user not found
        } catch (error) {
            throw new Error(`Failed to fetch user: ${error.message}`);
        }
    }

    static async logout(user_id){
        try {
            const user = await db('users').where({'user_id': user_id}).update({refresh_token : 'null'})
        } catch (error) {
            
        }
    }
    
    
    static async getUserByEmail(email) {
        try {
            const user = await db('users').where({'email' : email}).first();
            return user || null;
        } catch (error) {
            throw new Error(`Failed to fetch user: ${error.message}`);
        }
    }

    static async updateUser(id, updates) {
        const { email, username, password, role, status } = updates;
        const updated_at = new Date().toISOString();
        try {
            await db('users')
                .where({ id })
                .update({
                    email,
                    username,
                    password,
                    role,
                    status,
                    updated_at
                });
            return await this.getUserById(id); // Return updated user
        } catch (error) {
            throw new Error(`Failed to update user: ${error.message}`);
        }
    }

    static async deleteUser(id) {
        try {
            const user = await this.getUserById(id);
            if (!user) {
                throw new Error('User not found');
            }
            await db('users').where({ id }).del();
            return user;
        } catch (error) {
            throw new Error(`Failed to delete user: ${error.message}`);
        }
    }

    static async updateRefreshToken(email, malumot){
        try {
            await db('users').where({'email' : email}).update({'refresh_token' : malumot})
        } catch (error) {
            
        }
    }

    
}
