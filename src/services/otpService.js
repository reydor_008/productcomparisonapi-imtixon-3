import db from "../config/knexConnect.js";

export default class OtpService {
  static async createOtp(user_id, code) {
    try {
      const [id] = await db("otp")
        .insert({
          user_id,
          code,
        })
        .returning("id"); // Use .returning('id') to explicitly return the inserted ID

      return id;
    } catch (error) {
      throw new Error(`Failed to create OTP: ${error.message}`);
    }
  }

  static async getOtpByUserIdAndCode(userId, code) {
    try {
        console.log(userId, code)
      const otp = await db("otp")
        .where({ 'user_id': userId,  'code' : code })
        .orderBy("sended_time", "desc")
        .first();
      return otp;
    } catch (error) {
      throw new Error(`Failed to fetch OTP: ${error.message}`);
    }
  }

  static async getOtpById(id) {
    try {
      const code = await db("otp")
        .where({ uuid: id })
        .first()
        .returning("code");
      console.log(code);
      return code;
    } catch (error) {
      throw new Error(`Failed to fetch OTP: ${error.message}`);
    }
  }

  static async updateOtp(id, updates) {
    try {
      await db("otp").where({ id }).update(updates);
      return true; // Assuming update is successful
    } catch (error) {
      throw new Error(`Failed to update OTP: ${error.message}`);
    }
  }

  static async deleteOtp(id) {
    try {
      const otp = await this.getOtpById(id);
      if (!otp) {
        throw new Error("OTP not found");
      }
      await db("otp").where({ id }).del();
      return otp;
    } catch (error) {
      throw new Error(`Failed to delete OTP: ${error.message}`);
    }
  }
}
