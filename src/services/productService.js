import db from '../config/knexConnect.js';
import { v4 as uuidv4 } from 'uuid';

export default class ProductService {
    static async createProduct(product) {
        const id = uuidv4();
        const created_at = new Date().toISOString();
        product.id = id
        product.created_at = created_at
        console.log(product)
        try {
            await db('products').insert(
                product
            );
            return product;
        } catch (error) {
            
            throw new Error(`Failed to create product: ${error.message}`);
        }
    }

    static async getProductById(id) {
        try {
            const product = await db('products').where({ id }).first();
            return product;
        } catch (error) {
            throw new Error(`Failed to fetch product: ${error.message}`);
        }
    }
    static async getProduct() {
        try {
            const products = await db.select('*').from('products');
            return products;
        } catch (error) {
            throw new Error(`Failed to fetch product: ${error.message}`);
        }
    }


    static async updateProduct(id, updates) {
        const { name, description, category, price } = updates;
        const updated_at = new Date().toISOString();
        try {
            await db('products')
                .where({ id })
                .update({
                    name,
                    description,
                    category,
                    price,
                    updated_at
                });
            return await this.getProductById(id); // Return updated product
        } catch (error) {
            throw new Error(`Failed to update product: ${error.message}`);
        }
    }

    static async deleteProduct(id) {
        try {
            const product = await this.getProductById(id);
            if (!product) {
                throw new Error('Product not found');
            }
            await db('products').where({ id }).del();
            return product;
        } catch (error) {
            throw new Error(`Failed to delete product: ${error.message}`);
        }
    }
}
