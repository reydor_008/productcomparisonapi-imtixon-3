import dotenv from "dotenv"
dotenv.config()

export  const configuration = {
    server : {
        port : process.env.port
    },
    database : {
        db_host : process.env.db_host,
        db_port : process.env.db_port,
        db_password : process.env.db_password,
        db : process.env.db,
        db_user : process.env.db_user
    },
    jwt : {
        acces_token:{
            acces_token_key : process.env.jwt_acces_token_key,
            acces_token_time : process.env.jwt_acces_token_time,
            
        },
        refresh_token :{
            refresh_token_key : process.env.jwt_refresh_token_key,            
            refresh_token_time : process.env.jwt_refresh_token_time
        }
    }
}