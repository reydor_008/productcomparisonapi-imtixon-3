import knex from "knex";
import { configuration } from "./configuration.js";
const {database} = configuration

const db = knex({
    client : 'pg',
    connection : {
        port : database.db_port,
        host : database.db_host,
        password : database.db_password,
        database : database.db,
        user : database.db_user
    }
})


export default db
