import { Router } from "express";
import { createProduct, updateProduct, deleteProduct, getAllProducts, getOneProduct  } from "../controllers/productController.js";
import authencation from "../middlewares/authentication.js";
import { roleGuard } from "../middlewares/roleGuard.js";

const router = Router()

router.post('/create', authencation,roleGuard('admin', 'moderator'), createProduct)
router.get('/', authencation, getAllProducts)
router.get('/:id', authencation, getOneProduct)
router.put('/:id', authencation,roleGuard('admin', 'moderator'), updateProduct)
router.delete('/:id', authencation, roleGuard('admin', 'moderator'),deleteProduct)

export default router