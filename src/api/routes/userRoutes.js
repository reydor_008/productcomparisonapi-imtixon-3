import { Router } from "express";
import { createUser, getAllUsers, getUserByEmail, getCurrentUser, updateUser,deleteUser } from "../controllers/userController.js";
import { roleGuard } from "../middlewares/roleGuard.js";
import authencation from "../middlewares/authentication.js";

const router = Router()


router.post('/create',authencation, roleGuard('admin', 'moderator'), createUser)
router.get('/',  authencation, getAllUsers)
router.get('/:email',  authencation, getUserByEmail)
router.get('/me', authencation, getCurrentUser)
router.put('/:id', authencation, roleGuard('admin', 'moderator'), updateUser)
router.delete(':/id', authencation, roleGuard('admin', 'moderator'), deleteUser)

export default router