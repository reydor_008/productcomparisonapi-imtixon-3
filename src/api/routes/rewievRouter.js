import { Router } from "express";
import { createReview, getAllReiview, getOneReiview, updateReiview, deleteReiview } from "../controllers/rewievController.js";
import { roleGuard } from "../middlewares/roleGuard.js";
import authencation from "../middlewares/authentication.js";

const router = Router()

router.post('/create', authencation, roleGuard('admin', 'moderator'), createReview)
router.get('/', authencation, getAllReiview)
router.get('/:id', authencation, getOneReiview)
router.put('/:id', authencation, roleGuard('admin', 'moderator'), updateReiview)
router.delete('/:id', authencation, roleGuard('admin', 'moderator'), deleteReiview)

export default router   