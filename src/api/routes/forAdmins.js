import { Router } from "express";
import productRouter from './productRoutes.js'
import userRouter from './userRoutes.js'
import reviewRouter from './rewievRouter.js'

let router = Router()
router.use('/product', productRouter)
router.use('/user', userRouter)
router.use('/review', reviewRouter)


export default router