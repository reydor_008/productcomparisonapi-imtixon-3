import { Router } from "express";
import { signUp } from "../controllers/signUp.js";
import { isActive } from "../controllers/isActive.js";
import { signIn } from "../controllers/signInController.js";
import { getCurrentUser } from "../controllers/userController.js";
import { logout } from "../controllers/userController.js";
import authencation from "../middlewares/authentication.js";
import { updateRefresh } from "../controllers/refreshToken.js";
const router = Router()

router.post('/signup', signUp)
router.post('/verify-otp', isActive)
router.post('/signin', signIn)
router.get('/getme',authencation, getCurrentUser)
router.get('/logout',authencation, logout)
router.post('/refresh-token', updateRefresh)

export default router