import jwt from "jsonwebtoken"
import dotenv from "dotenv"
import ApiError from "../../errors/apiError.js"

dotenv.config()
const authencation = async (req, res, next) => {
    if(!req.headers.authorization){
        return res.status(401).send('JWT mavjud emas')
    }
    const [type, token] = req.headers.authorization.split(' ')
    console.log(type, token)
    if (type !== 'Bearer')
        return res.status(401).send('JWT Invalid')

    try {
        var decoded = jwt.verify(token, process.env.jwt_acces_token_key )
        
    } catch (error) {
        if (error instanceof jwt.TokenExpiredError) {
            return ApiError.token_expired( res, {
                errObject: error,
                status: error.status,
                msg: error.message,
                friendlyMsg: "Error logging out, please try again",
            })
        }
    }

    req.user = decoded

    next()
}

export default authencation