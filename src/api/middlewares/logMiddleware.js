import logger from "../../utils/logger.js";

const logMiddleware = async(req, res, next)=>{
    const struct = `Url:${req.url} Method:${req.method} Params : ${req.params}`
    logger.silly(struct)
    next()

}

export default logMiddleware