import UserService from "../../services/userService.js"
// import ApiError from "../../errors/apiError.js"
import ApiError from "../../errors/ApiError.js"
import Token from "../../utils/jwt.js"
import { configuration } from "../../config/configuration.js"
import { config } from "dotenv"



export const updateRefresh = async(req, res) =>{
   try {
     const {email} = req.body
     if(!email){
        return res.status(403).send("Email kiritilmagan")
     }
     let user = await UserService.getUserByEmail(email)
     if(!user){
         throw new Error("Siz ro'yxatda yo'qsiz")
     }
     const refreshToken = user.refresh_token
     if(!refreshToken){
         throw new Error('Siz logout Holatidasiz login qiling')
     }
     const malumotlar = await Token.tokenVerify(refreshToken, configuration.jwt.refresh_token.refresh_token_key)
     console.log("Malumotlar :", malumotlar)
     if(!malumotlar){
         throw new Error('Sizning refresh Tokeningiz eskirgan')
     }
     let payload = {
        userId : malumotlar.userId,
        role : malumotlar.role
     }
     const accesToken = await Token.tokenGenerator(payload, configuration.jwt.acces_token.acces_token_key, configuration.jwt.acces_token.acces_token_time)
     return res.status(201).json({acces_token: accesToken })
   } catch (error) {
    console.log(error)
    ApiError.unauthorized(res, {
        // errObject: error,
        status: 401,
        // msg: error.message,
        friendlyMsg: 'Invalid credentials'
    });
   }
}