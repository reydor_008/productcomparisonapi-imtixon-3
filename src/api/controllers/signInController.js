import bcrypt from 'bcrypt';
import jwt from 'jsonwebtoken';
import { configuration } from '../../config/configuration.js';
import UserService from '../../services/userService.js';
// import ApiError from '../../errors/apiError.js';
import ApiError from '../../errors/ApiError.js';
import { signInValidate } from '../../validators/signinValidate.js';

export const signIn = async (req, res) => {
    const {error} = await signInValidate(req.body)
    if(error){
        return res.status(401).send('Malumotlarni kiritishda xatolik mavjud')
    }

    const { email, password } = req.body;

    try {
        const user = await UserService.getUserByEmail(email);
        if (!user) {
            throw new Error('User not found');
        }

        const passwordMatch = await bcrypt.compare(password, user.password);
        if (!passwordMatch) {
            throw new Error('Incorrect password');
        }

        const accessToken = jwt.sign({ userId: user.id, role : user.role }, configuration.jwt.acces_token.acces_token_key, { expiresIn: '15m' });
        const refreshToken = jwt.sign({ userId: user.id, role : user.role  }, configuration.jwt.refresh_token.refresh_token_key, { expiresIn: '7d' });

        await UserService.updateRefreshToken(email, refreshToken)

        return res.status(200).json({ accessToken, refreshToken });


    } catch (error) {
        console.error('Error signing in:', error);
        ApiError.unauthorized(res, {
            errObject: error,
            status: 401,
            msg: error.message,
            friendlyMsg: 'Invalid credentials'
        });
    }
};
