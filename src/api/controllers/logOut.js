import { configuration } from "../../config/configuration.js";
import UserService from "../../services/userService.js";
import Token from "../../utils/jwt.js";
import ApiError from "../../errors/ApiError.js";
export const logOut = async (req, res) => {
  try {
    const [type, token] = req.headers.authorization.split(" ");
    const malumotlar = await Token.tokenVerify(
      token,
      configuration.jwt.acces_token.acces_token_key
    );
    const { user_id } = malumotlar;
    let user = await UserService.getUserById(user_id);
    if (!user) {
      throw new Error("Unathrized");
    }
    await UserService.logout(user_id);
    return res.status(200).send("Succesuful logouted");
  } catch (error) {
    ApiError.unauthorized(res, {
      errObject: error,
      status: 401,
      msg: error.message,
      friendlyMsg: "Invalid credentials",
    });
  }
};
