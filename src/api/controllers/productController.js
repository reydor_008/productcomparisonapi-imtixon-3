import ProductService from "../../services/productService.js";
import { productValidate } from "../../validators/validate.js";
import ApiError from "../../errors/ApiError.js";
import logger from '../../utils/logger.js';


export const createProduct = async (req, res) => {
    try {
        const { error } = await productValidate(req.body);
        if (error) {
            return ApiError.badRequest(res, "Invalid input data", "Data validation error");
        }

        const result = await ProductService.createProduct(req.body);
        if (result) {
            return res.status(201).json({ msg: "Product created successfully", data: result });
        }
    } catch (error) {
        logger.error(error.message)
        return ApiError.internal(res, error.message, "Failed to create product");
    }
};

export const getAllProducts = async (req, res) => {
    try {
        const products = await ProductService.getProduct();
        return res.status(200).json({ data: products });
    } catch (error) {
        return ApiError.internal(res, error.message, "Failed to fetch products");
    }
};

export const getOneProduct = async (req, res) => {
    try {
        const id = req.params.id;
        const product = await ProductService.getProductById(id);
        if (!product) {
            return ApiError.notFound(res, "Product not found", "No product with the given ID");
        }
        return res.status(200).json({ data: product });
    } catch (error) {
        return ApiError.internal(res, error.message, "Failed to fetch the product");
    }
};

export const updateProduct = async (req, res) => {
    try {
        const id = req.params.id;
        const { error } = await productValidate(req.body);
        if (error) {
            return ApiError.badRequest(res, "Invalid input data", "Data validation error");
        }

        const existingProduct = await ProductService.getProductById(id);
        if (!existingProduct) {
            return ApiError.notFound(res, "Product not found", "No product with the given ID");
        }

        const result = await ProductService.updateProduct(id, req.body);
        return res.status(200).json({ msg: "Product updated successfully", data: result });
    } catch (error) {
        return ApiError.internal(res, error.message, "Failed to update product");
    }
};

export const deleteProduct = async (req, res) => {
    try {
        const id = req.params.id;
        const result = await ProductService.deleteProduct(id);
        if (!result) {
            return ApiError.notFound(res, "Product not found", "No product with the given ID");
        }
        return res.status(200).json({ msg: "Product deleted successfully" });
    } catch (error) {
        return ApiError.internal(res, error.message, "Failed to delete product");
    }
};
