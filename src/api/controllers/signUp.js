import UserService from "../../services/userService.js";
import { userValidate } from '../../validators/validate.js'
import otpGenerator from "otp-generator";
import { main } from '../../utils/nodemailer.js'
import OtpService from "../../services/otpService.js";
// import ApiError from "../../errors/apiError.js";
import ApiError from "../../errors/ApiError.js";
import bcrypt from 'bcrypt'

export const signUp = async (req, res) => {
    try {
        const { error } = await userValidate(req.body);
        if (error) {
            return res.status(400).json({ message: "Malumotlarni kiritishda xato borda", data: error.details[0].message });
        }

        const hashedPassword = await bcrypt.hash(req.body.password, 14);
        const userData = {
            first_name : req.body.first_name,
            last_name : req.body.last_name,
            email: req.body.email,
            username: req.body.username,
            password: hashedPassword,
            role: req.body.role
        };

        const existingUser = await UserService.getUserByEmail(userData.email);
        if (existingUser) {
            throw new Error("Bunday malumotlar bilan foydalanuvchi royxatdan otgan");
        }

        
        const OTP = otpGenerator.generate(4, {
            upperCaseAlphabets: false,
            lowerCaseAlphabets: false,
            specialChars: false,
        });
        
        const createdUser = await UserService.createUser(userData);
        console.log(createdUser)
        const createOtp = await OtpService.createOtp(createdUser.id, OTP);

        if (!createOtp) {
            throw new Error("Otpni jonatishda xatolik mavjud");
        }

        await main(OTP, userData.email);

        return res.status(201).json({
            status: "Sign up is OK",
            message: "User created",
            userId: createdUser.id,
            otp: true
        });
    } catch (error) {
        console.error('Error in signUp controller:', error);
        ApiError.internal(res, {
            errObject: error,
            status: error.status,
            msg: error.message,
            friendlyMsg: "Bunday malumotlar bilan foydalanuvchi royxatdan otgan"
        });
    }
};
