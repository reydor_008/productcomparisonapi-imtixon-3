import jwt from "jsonwebtoken";
import { configuration } from "../../config/configuration.js"; // Example: Load JWT secrets from environment variables or a config file
import UserService from "../../services/userService.js";
import ApiError from "../../errors/apiError.js";
import { userValidate } from "../../validators/validate.js";

export const getCurrentUser = async (req, res) => {
  try {
    const token = req.headers.authorization?.split(" ")[1]; // Get token from "Bearer {token}"

    if (!token) {
      throw new Error("Authorization header missing");
    }

    const decodedToken = jwt.verify(
      token,
      configuration.jwt.acces_token.acces_token_key
    );
    const userId = decodedToken.userId;

    const user = await UserService.getUserById(userId);
    if (!user) {
      throw new Error("User not found");
    }

    const userData = {
      id: user.id,
      email: user.email,
      username: user.username,
      role: user.role,
      createdAt: user.created_at, 
      updatedAt: user.updated_at,
    };

    return res.status(200).json(userData);
  } catch (error) {
    console.error("Error fetching current user:", error);
    ApiError.unathorized(res, error.message, "Unauthorized");
  }
};

export const logout = async (req, res) => {
  try {
    const authHeader = req.headers.authorization;
    if (!authHeader) {
      throw new Error("Authorization header missing");
    }

    const token = authHeader.split(" ")[1];

    if (!token) {
      throw new Error("Token missing");
    }

    res.status(200).json({
      message: "Logout successful",
    });
  } catch (error) {
    console.error("Error in logout controller:", error);
    ApiError.internal(res, {
      errObject: error,
      status: error.status,
      msg: error.message,
      friendlyMsg: "Error logging out, please try again",
    });
  }
};

export const createUser = async (req, res) => {
  try {
    let { error } = await userValidate(req.body);
    if (error) {
      return res.status(405).send("Malumotlar chala kiritilgan");
    }
    let founded = await UserService.getUserByEmailAndUsername(
      req.body.email,
      req.body.username
    );
    if (founded) {
      return res
        .status(403)
        .send("Bunday malumotlar bilan foydalabuvchi royxatdan otgan");
    }
    let result = await UserService.createUser(req.body);
    if (!result) {
      throw new Error("Userni yaratishda xatolik");
    }
    return res.status(201).json({ data: result });
  } catch (error) {
    ApiError.internal(res, {
      errObject: error,
      status: error.status,
      msg: error.message,
      friendlyMsg: "Error logging out, please try again",
    });
  }
};

export const getAllUsers = async(req, res) =>{
    try {
        let users = await UserService.getAllUser()
        if(!users){
            return res.status(404).send("Userlar topilmadi")
        }
        return  res.status(200).json({data: users})
    } catch (error) {
        
    }
}

export const getUserByEmail = async(req, res) =>{
    try {
        let {email} = req.body
        if(!email){
            return res.status(403).send('Email is not defined')
        }
        const user = await UserService.getUserByEmail(email)
        if(!user){
            return res.status(404).send("Bunday malumotlar topilmadi")
        }
        return res.status(200).json({data:user})
    } catch (error) {
        
    }
}

export const updateUser = async(req, res) =>{
    let {error} = await userValidate(req.body)
    if(error){
        return res.status(402).send("Malumotlarni kiritishda xatolik mavjud")
    }
    let id = req.params.id
    let user = await UserService.updateUser(id, req.body)
    if(!user){
        throw new Error('Userni yangilashda xatolik mavjud')
    }
    return res.status(201).json({data:user})
}


export const deleteUser = async(req, res) =>{
  const id = req.params.id
  let user = await UserService.getUserById(id)
  if(!user){
    return res.status(404).send("Bunday malumotlar topilmadi")
  }
  let deletedUser = await UserService.deleteUser(id)
  return res.status(201).send("Succesuful deleted")

}