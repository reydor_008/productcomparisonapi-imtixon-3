import ReviewService from "../../services/rewievService.js";
import { reviewValidate } from "../../validators/validate.js";
import UserService from '../../services/userService.js'
import ProductService from "../../services/productService.js";
import ApiError from "../../errors/ApiError.js";
import logger from "../../utils/logger.js";
import Token from "../../utils/jwt.js"

export const createReview = async(req, res)=>{
    try {    
        let {error} = await reviewValidate(req.body)
        if(error){
            throw new Error(`Malumotlarni kiritishda xatolik bor ${error.details[0].message}`)
        }
        
        let {product_id} = req.body
        
        // let {user_id, product_id} = req.body

        // let foundUser = await UserService.getUserById(user_id)
        // if(!foundUser){
        //     throw new Error('Bunday User Royxatda yoq')
        // }

        const {userId} = req.user

        let foundProduct = await ProductService.getProductById(product_id)
        if(!foundProduct){
            throw new Error("Bunday Product Ro'yxatda yo'q")
        }
        const reviewData = {
            ...req.body,
            user_id : userId
        }

        let result = await ReviewService.createReview(reviewData)
        if(result){
            return res.status(201).json({msg : 'Malumotlar qoshildi', data : result})
        }
    } catch (error) {
        logger.error(error.message)
        ApiError.badRequest(res, error.message, "Notogri malumotlar kiritildi")
    }
}

export const getAllReiview = async(req, res) =>{
    try {
        const rewievs = await ReviewService.getReview()
        return res.status(200).json({data:rewievs})
    } catch (error) {
    }
}

export const getOneReiview = async(req, res) =>{
    let id = req.params.id
    let product = await ReviewService.getReviewById(id)
    if(!product){
        return res.status(404).send("Bunday maxsulot topilmadi")
    }
    return res.status(200).json({product})
}

export const updateReiview = async(req, res) =>{
    let id = req.params.id
    let {error} = await reviewValidate(req.body)
    if(error){
        return res.status(403).send("Malumotlarni kiritishda xatolik mavjud")
    }

    let founded = await ReviewService.getReviewById(id)
    if(!founded){
        return res.status(404).send("MAlumotlar topilmadi")
    }

    let result = await ReviewService.updateReview(id, req.body)

    res.status(201).json({result})
    
}

export const deleteReiview = async(req, res) =>{
    let id = req.params.id
    let result = await ReviewService.deleteReview(id)
    if(!result){
        return res.status(404).send("Bunday malumotlar topilmadi")
    }
    return res.status(201).send("Malumotlar ochirildi")
}