import OtpService from '../../services/otpService.js';
import UserService from '../../services/userService.js';
import ApiError from '../../errors/apiError.js';

export const isActive = async (req, res) => {
    const { userId, code } = req.body;

    try {
        const otpRecord = await OtpService.getOtpByUserIdAndCode(userId, code);

        if (!otpRecord) {
            throw new Error('Invalid OTP');
        }

        await UserService.activateUser(userId);

        return res.status(200).json({ message: "OTP verified, account activated" });
    } catch (error) {
        console.error('Error verifying OTP:', error);
        ApiError.internal(res, {
            errObject: error,
            status: 500,
            msg: error.message,
            friendlyMsg: "Failed to verify OTP and activate account"
        });
    }
};
