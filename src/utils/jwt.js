import jwt from "jsonwebtoken";
import { configuration } from "../config/configuration.js";


export default class Token {
  static async tokenGenerator(userData, secretKey, expireIn) {
    console.log(expireIn)
    const token =  jwt.sign(userData, secretKey, {expiresIn: expireIn}); // bu yerda userData payloa bolyapti
    return token; //tayyor tokenni qaytaryapmiz
  }
  static async tokenVerify(token, secretKey) {
    if (!token || !secretKey) {
      return false;
    }
    // console.log(token, secretKey);
    try {
      const decoded =  jwt.verify(token, secretKey);
      return decoded;
    } catch (error) {
      
      return false;
    }
  }
}
