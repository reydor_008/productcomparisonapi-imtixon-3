import nodemailer from "nodemailer"
import dotenv from "dotenv"
dotenv.config()

// console.log(process.env.My_email, process.env.Node_emailer_pass)

const transporter = nodemailer.createTransport({
    host: "smtp.gmail.com",
    port: 465,
    secure: true,
    auth: {
        user: process.env.NODEMAILER_EMAIL, // sizning emailingiz bo'lishi kerak
        pass: process.env.NODEMAILER_PASSWORD // google acountizdan olgan app passwordingiz bo'ladi
    }
})

export  async function main(otp, email) {

    const info = await transporter.sendMail({
        to: email,
        subject: "3 daqiqalik kodingiz",
        text: "Hello world",
        html: `
            <h1>  Assalomu alaykum sizning 1 daqiqalik kodingiz: ${otp}  </h1>
        `
    })

    console.log("Message sent: %s", info.messageId);
}
