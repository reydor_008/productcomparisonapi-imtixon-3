import express from "express"
import logMiddleware from "./api/middlewares/logMiddleware.js"
import PgModel from "./models/models.js"
import authRouter from './api/routes/authRoutes.js'
import adminRouter from './api/routes/forAdmins.js'
import { roleGuard } from "./api/middlewares/roleGuard.js"
import authencation from "./api/middlewares/authentication.js"
// import indexRouter from './api/routes/indexRoutes.js'

export const app = express()

const Model =  new PgModel()

app.use(express.json())
app.use(logMiddleware)
app.use('/auth', authRouter)
app.use('/api', authencation, roleGuard('admin', 'moderator', 'user'), adminRouter)
